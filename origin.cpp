/*
  Zetrix
  Version 1.0
*/

#include <cstdlib>
#include <SDL.h>

static const char SQUARE_SIZE = 20; //pixels

// Intervals
#define INTERVAL      500 //milliseconds (initial update rate)
#define SPEED_CHANGE  50 //milliseconds
#define FPS_INTERVAL  5000 //milliseconds

// Dificulty Interval Modifiers
#define SLOW_DIFF     300
#define FAST_DIFF     300

// Score per Second
#define SCORE_TIME  1

// Score per Line
#define EASY_SCORE_TIME 5
#define NORM_SCORE_LINE 10
#define FAST_SCORE_LINE 20
static int score_line = NORM_SCORE_LINE;

// Background Color
static char back_r = 0, back_g = 0, back_b = 0;

// Graphic Quality (true = pictures, false = just squares)
static bool quality = true;

// Image Paths
#define IMG_ICON    "data/icon.bmp"
#define IMG_TITLE   "data/zetrix.bmp"
#define IMG_START   "data/menu.bmp"
#define IMG_PAUSED  "data/pause.bmp"
#define IMG_OVER    "data/over.bmp"
#define IMG_VERSION "data/version.bmp"
#define IMG_BLOCKS  "data/blocks.bmp"

// Block ID's
#define NONE    0 //background
#define RED     1 //a
#define ORANGE  2 //b
#define YELLOW  3 //c
#define GREEN   4 //d
#define CYAN    5 //e
#define BLUE    6 //f
#define PURPLE  7 //g
#define WHITE   8 //color before line deleted
#define GRAY    9 //color on game over

// Block Shape (0=0, 1=90, 2=180, 3=270, counter clock wise: left)
typedef char shape[4][4];

shape
shape_z[4] = {
 {{0,0,0,0},
  {0,0,0,0},
  {0,0,0,0},
  {0,0,0,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {0,0,0,0},
  {0,0,0,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {0,0,0,0},
  {0,0,0,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {0,0,0,0},
  {0,0,0,0}}},

shape_a[4] = {
 {{0,0,0,0},
  {0,0,0,0},
  {0,1,1,0},
  {0,1,1,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {0,1,1,0},
  {0,1,1,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {0,1,1,0},
  {0,1,1,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {0,1,1,0},
  {0,1,1,0}}},

shape_b[4] = {
 {{0,1,0,0},
  {0,1,0,0},
  {0,1,0,0},
  {0,1,0,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {1,1,1,1},
  {0,0,0,0}},

 {{0,1,0,0},
  {0,1,0,0},
  {0,1,0,0},
  {0,1,0,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {1,1,1,1},
  {0,0,0,0}}},

shape_c[4] = {
 {{0,0,0,0},
  {0,1,0,0},
  {1,1,1,0},
  {0,0,0,0}},

 {{0,0,0,0},
  {0,1,0,0},
  {1,1,0,0},
  {0,1,0,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {1,1,1,0},
  {0,1,0,0}},

 {{0,0,0,0},
  {0,1,0,0},
  {0,1,1,0},
  {0,1,0,0}}},

shape_d[4] = {
 {{0,0,0,0},
  {0,0,0,0},
  {1,1,0,0},
  {0,1,1,0}},

 {{0,0,0,0},
  {0,0,1,0},
  {0,1,1,0},
  {0,1,0,0}},

 {{0,0,0,0},
  {1,1,0,0},
  {0,1,1,0},
  {0,0,0,0}},

 {{0,0,0,0},
  {0,1,0,0},
  {1,1,0,0},
  {1,0,0,0}}},

shape_e[4] = {
 {{0,0,0,0},
  {0,0,0,0},
  {0,1,1,0},
  {1,1,0,0}},

 {{0,0,0,0},
  {0,1,0,0},
  {0,1,1,0},
  {0,0,1,0}},

 {{0,0,0,0},
  {0,1,1,0},
  {1,1,0,0},
  {0,0,0,0}},

 {{0,0,0,0},
  {1,0,0,0},
  {1,1,0,0},
  {0,1,0,0}}},

shape_f[4] = {
 {{0,0,0,0},
  {0,1,0,0},
  {0,1,0,0},
  {0,1,1,0}},

 {{0,0,0,0},
  {0,0,1,0},
  {1,1,1,0},
  {0,0,0,0}},

 {{0,0,0,0},
  {1,1,0,0},
  {0,1,0,0},
  {0,1,0,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {1,1,1,0},
  {1,0,0,0}}},

shape_g[4] = {
 {{0,0,0,0},
  {0,0,1,0},
  {0,0,1,0},
  {0,1,1,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {0,1,1,1},
  {0,0,0,1}},

 {{0,0,0,0},
  {0,0,1,1},
  {0,0,1,0},
  {0,0,1,0}},

 {{0,0,0,0},
  {0,1,0,0},
  {0,1,1,1},
  {0,0,0,0}}};

// Block (made out of 4 squares)
typedef struct {
  int x;
  int y;
  int rot;
  int color;
  shape s;
} block;

// Returns the numbered Color
int getColor(SDL_Surface *target, char color) {
  char r = 0, g = 0, b = 0;

  switch(color) {
    case NONE:
      r = 32; g = 32; b = 32;
      break;

    case RED:
      r = 200; g = 0; b = 0;
      break;

    case ORANGE:
      r = 255; g = 128; b = 0;
      break;

    case YELLOW:
      r = 255; g = 255; b = 0;
      break;

    case GREEN:
      r = 0; g = 255; b = 0;
      break;

    case CYAN:
      r = 0; g = 200; b = 255;
      break;

    case BLUE:
      r = 0; g = 64; b = 200;
      break;

    case PURPLE:
      r = 128; g = 0; b = 128;
      break;

    case WHITE:
      r = 230; g = 230; b = 230;
      break;

    default: // GRAY
      r = 64; g = 64; b = 64;
  }

  return SDL_MapRGB(target -> format, r, g, b);
}


// Draw a Square
void draw_square(SDL_Surface *target, SDL_Surface *blocks, SDL_Rect *size, int color) {
  if(quality) {
    SDL_Rect a, b;
    a.x = color * SQUARE_SIZE; a.y = 0; a.w = size -> w; a.h = size -> h;
    b.x = size -> x; b.y = size -> y; b.w = 0; b.h = 0;

    SDL_BlitSurface(blocks, &a, target, &b);
  } else
    SDL_FillRect(target, size, getColor(target, color));
}


// Draw a Block
void draw_block(SDL_Surface *target, SDL_Surface *blocks, block b) {
  SDL_Rect temp;
  temp.w = SQUARE_SIZE; temp.h = SQUARE_SIZE;

  int i, j;
  for(i = 0; i < 4; i++) {
    for(j = 0; j < 4; j++) {
      if(b.s[i][j]) {
        temp.x = (b.x + i) * (SQUARE_SIZE + 1) + 1;
        temp.y = (b.y + j) * (SQUARE_SIZE + 1) + 1;
        if(temp.y >= 0)
          draw_square(target, blocks, &temp, b.color);
      }
    }
  }
}


// Copy Shape
void copy_shape(shape src, shape dst) {
  int i, j;
  for(i = 0; i < 4; i++) {
    for(j = 0; j < 4; j++) {
      dst[j][i] = src[i][j];
    }
  }
}


// Make a Random Block
void make_random_block(block *b) {
  b -> x = 3;
  b -> y = -4;
  b -> rot = 0;

  int tmp = (rand() % 7) + 1;
  b -> color = tmp;

  switch(tmp) {
    case 1: copy_shape(shape_a[0], b -> s); break;
    case 2: copy_shape(shape_b[0], b -> s); break;
    case 3: copy_shape(shape_c[0], b -> s); break;
    case 4: copy_shape(shape_d[0], b -> s); break;
    case 5: copy_shape(shape_e[0], b -> s); break;
    case 6: copy_shape(shape_f[0], b -> s); break;
    case 7: copy_shape(shape_g[0], b -> s); break;
    default: copy_shape(shape_z[0], b -> s); break;
  }
}


// Check for Side Collision
bool check_side_coll(block bl, char bo[20][10]) {
  int i, j, ta, tb;
  for(i = 0; i < 4; i++) {
    for(j = 0; j < 4; j++) {
      if(bl.s[i][j]) {
        ta = bl.x + i;
        tb = bl.y + j;

        // Side Wall Collision
        if(ta < 0 || ta > 9)
          return true;

        // Square Collision
        if(bo[tb][ta])
          return true;
      }
    }
  }
  return false;
}


// Check for Bottom Collision
bool check_bottom_coll(block bl, char bo[20][10]) {
  int i, j, ta, tb;
  for(i = 0; i < 4; i++) {
    for(j = 0; j < 4; j++) {
      if(bl.s[i][j]) {
        ta = bl.x + i;
        tb = bl.y + j;

        // Bottom Wall Collision
        if(tb > 19)
          return true;

        // Square Collision
        if(bo[tb][ta])
          return true;
      }
    }
  }
  return false;
}


// Rotate Block
void rotate_block(block *b, bool right) {
  if(right)
    b -> rot--;
  else
    b -> rot++;

  if(b -> rot < 0)
    b -> rot = 3;

  if(b -> rot > 3)
    b -> rot = 0;

  switch(b -> color) {
    case 1: copy_shape(shape_a[b -> rot], b -> s); break;
    case 2: copy_shape(shape_b[b -> rot], b -> s); break;
    case 3: copy_shape(shape_c[b -> rot], b -> s); break;
    case 4: copy_shape(shape_d[b -> rot], b -> s); break;
    case 5: copy_shape(shape_e[b -> rot], b -> s); break;
    case 6: copy_shape(shape_f[b -> rot], b -> s); break;
    case 7: copy_shape(shape_g[b -> rot], b -> s); break;
    default: copy_shape(shape_z[b -> rot], b -> s); break;
  }
}


// Merge a Block with the Board
void merge_squares(block bl, char bo[20][10]) {
  int i, j;
  for(i = 0; i < 4; i++) {
    for(j = 0; j < 4; j++) {
      if(bl.s[i][j] && (bl.y + j >= 0) && (bl.y + j < 20) && (bl.x + i >= 0) && (bl.x + i < 10))
        bo[bl.y + j][bl.x + i] = bl.color;
    }
  }
}


// Clear the Surface
void clear_surf(SDL_Surface *target) {
  SDL_FillRect(target, &target -> clip_rect, SDL_MapRGB(target -> format, back_r, back_g, back_b));
}

// Clear the Board
void clear_board(char b[20][10]) {
  int i, j;
  for(j = 0; j < 20; j++)
    for(i = 0; i < 10; i++)
      b[j][i] = 0;
}


// Turn all the Blocks Gray (Game Over)
void gray_board(char b[20][10]) {
  int i, j;
  for(j = 0; j < 20; j++)
    for(i = 0; i < 10; i++)
      if(b[j][i])
        b[j][i] = GRAY;
}


// Turns a Line White (marks it for elimination)
void white_line(char board[20][10], int position) {
  int i;
  for(i = 0; i < 10; i++)
    board[position][i] = WHITE;
}


// Moves all the Blocks down
void slide(char board[20][10], int position) {
  int i, j;
  for(j = position; j >= 0; j--)
    for(i = 0; i < 10; i++)
      if(j == 0)
        board[j][i] = 0;
      else
        board[j][i] = board[j-1][i];
}


// Erases all the White Lines
void sweep_board(char board[20][10]) {
  int j;
  for(j = 0; j < 20; j++)
    if(board[j][0] == WHITE)
      slide(board, j);
}


// Checks for lines and slide over them
int check4lines(char board[20][10], bool *sweep) {
  int i, j, line, changes = 0;
  for(j = 0; j < 20; j++) {
    line = 0;

    for(i = 0; i < 10; i++)
      if(board[j][i])
        line++;

    if(line == 10) {
      white_line(board, j);
      *sweep = true;
      changes++;
    }
  }
  return changes;
}


// Check if the Game is Over
bool check4lose(char board[20][10]) {
  int i;
  for(i = 0; i < 10; i++)
    if(board[0][i])
      return true;
  return false;
}


// Define a SDL_Rect
void setRect(SDL_Rect *r, int x, int y, int w, int h) {
  r -> x = x;
  r -> y = y;
  r -> w = w;
  r -> h = h;
}


// Load BMP
SDL_Surface* loadbmp(char *file) {
  SDL_Surface *raw_image = NULL, *image = NULL;
  raw_image = SDL_LoadBMP(file);
  if(!raw_image)
    return NULL;

  image = SDL_DisplayFormat(raw_image);
  if(!image)
    return NULL;

  Uint32 ckey = SDL_MapRGB(image -> format, 0, 0, 0);
  SDL_SetColorKey(image, SDL_SRCCOLORKEY, ckey);

  return image;
}


// Save a Screen Shot
void screen_shot(SDL_Surface *target) {
  int i = 0;
  char output[100];
  FILE *f = NULL;

  f = fopen("screen_0.bmp", "r");
  sprintf(output, "screen_0.bmp");
  while(f) {
    fclose(f);
    i++;
    sprintf(output, "screen_%d.bmp", i);
    f = fopen(output, "r");
  }

  SDL_SaveBMP(target, output);
}


int main (int argc, char** argv) {
  // SDL Init
  if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) == -1) {
    fprintf(stderr, "Error: Unable to Initialize SDL, %s.\n", SDL_GetError());
    return 1;
  }

  // Caption Init
  char caption[40];
  sprintf(caption, "Zetrix");
  SDL_WM_SetCaption(caption, caption);

  // Icon Load and Init
  SDL_Surface *img_icon = NULL;
  img_icon = SDL_LoadBMP(IMG_ICON);

  if(!img_icon) {
    fprintf(stderr, "Error: Unable to load the Icon file, %s.\n", SDL_GetError());
    return 3;
  }

  SDL_WM_SetIcon(SDL_LoadBMP(IMG_ICON), NULL);

  // Video Mode Init
  SDL_Surface *screen = NULL;
  screen = SDL_SetVideoMode(10 * SQUARE_SIZE + 11, 20 * SQUARE_SIZE + 21, 0, SDL_SWSURFACE);
  if(!screen) {
    fprintf(stderr, "Error: Unable to set the Video Mode, %s.\n", SDL_GetError());
    return 2;
  }

  // Load Images
  SDL_Surface *img_title = loadbmp(IMG_TITLE),
  *img_start = loadbmp(IMG_START),
  *img_paused = loadbmp(IMG_PAUSED),
  *img_over = loadbmp(IMG_OVER),
  *img_ver = loadbmp(IMG_VERSION),
  *img_blocks = loadbmp(IMG_BLOCKS);

  if(!img_title || !img_start || !img_paused || !img_over || !img_ver || !img_blocks) {
    fprintf(stderr, "Error: Unable to load an Image file, %s.\n", SDL_GetError());
    return 4;
  }

  if(img_blocks -> h != SQUARE_SIZE) {
    quality = false;
    fprintf(stderr, "Warning: Block Image not suited for the Block Size, switching to low quality.\n");
  }

  if(img_title -> w > screen -> w || img_start -> w > screen -> w || img_paused -> w > screen -> w || img_over -> w > screen -> w)
    fprintf(stderr, "Warning: Menu image(s) is\\are too big.\n");

  // Generate Random Seed
  srand(SDL_GetTicks());

  // Modify the Key Repeat Rate
  SDL_EnableKeyRepeat(100, 100);

  // Score & Board Variables
  int score = 0;
  char board[20][10];
  bool sweep = false;

  // Cycle Variables (avoids making them per frame)
  int i, j;

  // Block Rect
  SDL_Rect square;
  square.x = 0; square.y = 0;
  square.w = SQUARE_SIZE; square.h = SQUARE_SIZE;

  // Update Variables
  bool update = false;
  int interval = INTERVAL;
  Uint32 counter = SDL_GetTicks(), pause_start = 0;

  SDL_Event event;
  bool done = false, pause = false, freeze = false;

  // Menu Variables and Image Location (Centered) Init
  bool menu = true, over = false;
  SDL_Rect src_rect, dst_rect;
  int title_x = (screen -> w / 2) - (img_title -> w / 2),
      title_y = (screen -> h / 6) - (img_title -> h / 2),
      start_x = (screen -> w / 2) - (img_start -> w / 2),
      start_y = (int) ((screen -> h / 6) * 3.5 - (img_start -> h / 2)),
      paused_x = (screen -> w / 2) - (img_paused -> w / 2),
      paused_y = (screen -> h / 6) * 3 - (img_paused -> h / 2),
      over_x = (screen -> w / 2) - (img_over -> w / 2),
      over_y = (screen -> h / 6) * 3 - (img_over -> h / 2),
      version_x = (screen -> w / 2) - (img_ver -> w / 2),
      version_y = (screen -> h) - (img_ver -> h * 2);


  // Block Variables
  block b;

  // FPS Counting Variables
  Uint32 fps_start = 0, fps_count = 0;
  const int fps_interval = FPS_INTERVAL / 1000;
  bool count_fps = false;

  // Difficulty Variables (0=SLOW, 1=NORM, 2=FAST)
  char diff = 0;

  // Clear the Screen
  clear_surf(screen);

  // Main Loop
  while(!done) {
    if(freeze) {
      // Wait for and Handle Input
      if(SDL_WaitEvent(&event)) {
        switch(event.type) {
          case SDL_QUIT:
            done = true;
            break;

          case SDL_ACTIVEEVENT:
            if(event.active.state & SDL_APPACTIVE) {
              if(event.active.gain) {
                freeze = false;

                if(menu)
                  sprintf(caption, "Zetrix");
                else if(pause)
                  sprintf(caption, "Zetrix (Paused)");
                else
                  sprintf(caption, "Zetrix (%d pts)", score);

                SDL_WM_SetCaption(caption, caption);
              }
            }
            break;
        }
      }

    } else {

    // Handle Input
    while(SDL_PollEvent(&event)) {
      switch(event.type) {
        case SDL_QUIT:
          done = true;
          break;

        case SDL_ACTIVEEVENT:
          if(event.active.state & SDL_APPACTIVE) {
            if(!event.active.gain) {
              sprintf(caption, "Zetrix (Minimized)");
              SDL_WM_SetCaption(caption, caption);
              freeze = true;
            }
          }
          break;

        case SDL_KEYDOWN:
          switch(event.key.keysym.sym) {
            case SDLK_ESCAPE: case SDLK_x:
              if(menu)
                done = true;
              else {
                clear_board(board);
                clear_surf(screen);
                menu = true;
                over = false;
                pause = false;
                sweep = false;
                sprintf(caption, "Zetrix");
                SDL_WM_SetCaption(caption, caption);
              }
              break;

            case SDLK_1: case SDLK_2: case SDLK_3:
              if(menu) {
                if(event.key.keysym.sym == SDLK_1) {
                  diff = 0;
                  interval = INTERVAL + SLOW_DIFF;
                } else if(event.key.keysym.sym == SDLK_2) {
                  diff = 1;
                  interval = INTERVAL;
                } else if(event.key.keysym.sym == SDLK_3) {
                  diff = 2;
                  interval = INTERVAL - FAST_DIFF;
                }

                menu = false;
                over = false;
                pause = false;
                sweep = false;
                score = 0;
                counter = SDL_GetTicks();
                sprintf(caption, "Zetrix (0 pts)");
                SDL_WM_SetCaption(caption, caption);
                make_random_block(&b);
                clear_board(board);
                clear_surf(screen);
              }
              break;

            case SDLK_p:
              if(!menu && !over) {
                if(pause) {
                  pause = false;
                  clear_surf(screen);
                  counter += (SDL_GetTicks() - pause_start);
                  sprintf(caption, "Zetrix (%d pts)", score);
                  SDL_WM_SetCaption(caption, caption);
                } else {
                  pause = true;
                  pause_start = SDL_GetTicks();
                  sprintf(caption, "Zetrix (Paused)");
                  SDL_WM_SetCaption(caption, caption);
                }
              }
              break;

            case SDLK_q:
              quality = !quality;
              break;

            case SDLK_LEFT: case SDLK_a:
              if(!menu && !over && !pause) {
                b.x--;
                if(check_side_coll(b, board))
                  b.x++;
              }
              break;

            case SDLK_RIGHT: case SDLK_d:
              if(!menu && !over && !pause) {
                b.x++;
                if(check_side_coll(b, board))
                  b.x--;
              }
              break;

            case SDLK_UP: case SDLK_w:
              if(!menu && !over && !pause) {
                rotate_block(&b, true);
                if(check_side_coll(b, board))
                  rotate_block(&b, false);
              }
              break;

            case SDLK_DOWN: case SDLK_s:
              if(!menu && !over && !pause)
                interval = SPEED_CHANGE;
              break;

            case SDLK_f:
              if(count_fps)
                count_fps = false;
              else {
                count_fps = true;
                fps_start = SDL_GetTicks();
                fps_count = 0;
              }
              break;

            case SDLK_PRINT: case SDLK_r:
              screen_shot(screen);
              break;

            default:
              if(over) {
                clear_board(board);
                clear_surf(screen);
                menu = true;
                over = false;
                pause = false;
                sweep = false;
                sprintf(caption, "Zetrix");
                SDL_WM_SetCaption(caption, caption);
              }
              break;
          }
          break;

          case SDL_KEYUP:
            switch(event.key.keysym.sym) {
              case SDLK_DOWN: case SDLK_s:
                if(!menu && !over && !pause) {
                  switch(diff) {
                    case 0:
                      interval = INTERVAL + SLOW_DIFF;
                      break;

                    case 1:
                      interval = INTERVAL;
                      break;

                    case 2:
                      interval = INTERVAL - FAST_DIFF;
                      break;
                  }
                }
              break;

              default:
                break;
            }
          break;
      }
    }

    if(menu) {
      // Draw Menu
      setRect(&src_rect, 0, 0, img_title -> w, img_title -> h);
      setRect(&dst_rect, title_x, title_y, 0, 0);
      SDL_BlitSurface(img_title, &src_rect, screen, &dst_rect);

      setRect(&src_rect, 0, 0, img_start -> w, img_start -> h);
      setRect(&dst_rect, start_x, start_y, 0, 0);
      SDL_BlitSurface(img_start, &src_rect, screen, &dst_rect);

      setRect(&src_rect, 0, 0, img_ver -> w, img_ver -> h);
      setRect(&dst_rect, version_x, version_y, 0, 0);
      SDL_BlitSurface(img_ver, &src_rect, screen, &dst_rect);
    } else {
      // Draw Squares
      if(quality)
        clear_surf(screen);

      for(j = 0; j < 20; j++) {
        square.y = j * (SQUARE_SIZE+1) + 1;
        for(i = 0; i < 10; i++) {
          square.x = i * (SQUARE_SIZE+1) + 1;
          draw_square(screen, img_blocks, &square, board[j][i]);
        }
      }

      // Draw Block
      draw_block(screen, img_blocks, b);

      if(pause) {
        // Draw "Paused"
        setRect(&src_rect, 0, 0, img_paused -> w, img_paused -> h);
        setRect(&dst_rect, paused_x, paused_y, 0, 0);
        SDL_BlitSurface(img_paused, &src_rect, screen, &dst_rect);
      } else if(over) {
      // Draw "Game Over"
        setRect(&src_rect, 0, 0, img_over -> w, img_over -> h);
        setRect(&dst_rect, over_x, over_y, 0, 0);
        SDL_BlitSurface(img_over, &src_rect, screen, &dst_rect);
      } else {
        // Update
        if(SDL_GetTicks() > (counter + interval)) {
          counter = SDL_GetTicks();
          update = true;
        }

        if(update) {
          update = false;

          if(sweep) {
            sweep_board(board);
            sweep = false;
          } else {
            b.y++;
            if(check_bottom_coll(b, board)) {
              b.y--;
              merge_squares(b, board);
              make_random_block(&b);
            }

            if(check4lose(board)) {
              clear_surf(screen);
              over = true;
              gray_board(board);
            } else {
              score += SCORE_TIME + check4lines(board, &sweep) * score_line;
              sprintf(caption, "Zetrix (%d pts)", score);
              SDL_WM_SetCaption(caption, caption);
            }
          }
        }
      }
    }

    SDL_Flip(screen);

    if(count_fps) {
      fps_count++;
      if(SDL_GetTicks() > fps_start + FPS_INTERVAL) {
        printf("%d frames in %d second(s) = %d frames per second\n", fps_count, fps_interval, fps_count / fps_interval);
        fps_start = SDL_GetTicks();
        fps_count = 0;
      }
    }

    }
  }

  SDL_FreeSurface(screen);
  SDL_FreeSurface(img_blocks);
  SDL_FreeSurface(img_ver);
  SDL_FreeSurface(img_over);
  SDL_FreeSurface(img_paused);
  SDL_FreeSurface(img_start);
  SDL_FreeSurface(img_title);
  SDL_FreeSurface(img_icon);
  SDL_Quit();
  return 0;
}

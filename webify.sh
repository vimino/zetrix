#!/bin/sh
FILE=zetrix.cpp
source activate
emcc "$FILE" -s WASM=1 -s USE_SDL=2 -s USE_SDL_IMAGE=2 -s SDL2_IMAGE_FORMATS='["png"]' --preload-file data -s LINKABLE=1 -s EXPORT_ALL=1 -s MODULARIZE=1 -o index.js
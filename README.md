#### Zetrix

![Screenshot](screenshot.png)

A simple [C++](https://en.wikipedia.org/wiki/C%2B%2B) [Tetris](https://en.wikipedia.org/wiki/Tetris) clone, made with [SDL 1](https://www.libsdl.org/download-1.2.php) back in 2008. Quite a trip down memory lane.

I left the original source in "origin.cpp", which loads *BMP*s (no need for [SDL_Image](https://www.libsdl.org/projects/SDL_image/)) and an annoying bug that only lets it generate 2 types of pieces (shape_d and shape_f).
Upon revisiting it I leared that it was due to a segmentation fault that made the `check_bottom_coll` function return true before the piece entered the game. Got to love **C** and **C++** for letting its arrays access *anything*. Though, technically, they're just pointers.

I decided to update it and use [Emscripten](https://emscripten.org/) to bring it to the web, by using [WebAssembly](https://webassembly.org/). Personally I could see why I haven't touched **C++** for a long time as this conversion alone took me around 1 week and I find that excessive for such a simple game. So, what changes did I have to do?
- [Get Emscripten](https://emscripten.org/docs/getting_started/downloads.html)
- Fix Bug
- Convert to SDL2 (added *SDL_Image* to avoid conversions, since *SDL_LoadBMP* returns a *SDL_Surface*)
- Split `loop` from `main`
- [Learn how to make the **webify** script (webify.sh)](https://emscripten.org/docs/getting_started/Tutorial.html). How to use Emscripten's *emcc* and compile C++ files into **JS+WASM** files.
- Test several flags to see what works (requires the most research/tests)
- [Learn how to make the page (index.html)](https://hub.packtpub.com/creating-and-loading-a-webassembly-module-with-emscriptens-glue-code-tutorial/) as Emscripten's one (made if you use `-o index.html`) didn't work

Note that I couldn't get the [Tests](https://emscripten.org/docs/getting_started/test-suite.html) running with ease and mostly looked for solutions elsewhere.
There's also an unexpected border but ignore it.

---

## [Play it, here!](https://vimino.gitlab.io/zetrix)

#### Controls:
- **Up Arrow or W** - Rotate
- **Left Arrow or A** - Move left
- **Right Arrow or D** - Move right
- **Down Arrow or S** - Speed Up
- **Spacebar or Enter** - Exit Game Over screen (works with other keys)
- **Escape or X** - Return to Menu / Exit
- **Q** - Toggle between drawing Images or Squares (only on Original)
- **F** - Toggle FPS  (only on Original)
- **PrintScreen or R** - Take Screenshot (only on Original)

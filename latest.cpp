/*
  Zetrix
  Version 1.1 - 2021 (C++ 2011)
*/

#include <stdlib.h>
#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#if __EMSCRIPTEN__
    #include <emscripten.h>
#endif

static const char SQUARE_SIZE = 20; //pixels

// Intervals
#define INTERVAL      500 //milliseconds (initial update rate)
#define SPEED_CHANGE  50 //milliseconds
#define FPS_INTERVAL  5000 //milliseconds

// Dificulty Interval Modifiers
#define SLOW_DIFF     300
#define FAST_DIFF     300

// Score per Second
#define SCORE_TIME  1

// Score per Line
#define EASY_SCORE_TIME 5
#define NORM_SCORE_LINE 10
#define FAST_SCORE_LINE 20
static int score_line = NORM_SCORE_LINE;

// Background Color
static char back_r = 0, back_g = 0, back_b = 0;

// Image Paths
const char* IMG_ICON = "data/icon.bmp";
int* IMG_ICON_SIZE = new int[2];
const char* IMG_TITLE = "data/zetrix.png";
int* IMG_TITLE_SIZE = new int[2];
#if __EMSCRIPTEN__
    const char* IMG_START = "data/menu2.png";
#else
    const char* IMG_START = "data/menu.png";
#endif
int* IMG_START_SIZE = new int[2];
const char* IMG_PAUSED = "data/pause.png";
int* IMG_PAUSED_SIZE = new int[2];
const char* IMG_OVER = "data/over.png";
int* IMG_OVER_SIZE = new int[2];
const char* IMG_VERSION = "data/version.png";
int* IMG_VERSION_SIZE = new int[2];
const char* IMG_BLOCKS = "data/blocks.png";
int* IMG_BLOCKS_SIZE = new int[2];

// Block ID's
#define NONE    0 //background
#define RED     1 //a
#define ORANGE  2 //b
#define YELLOW  3 //c
#define GREEN   4 //d
#define CYAN    5 //e
#define BLUE    6 //f
#define PURPLE  7 //g
#define WHITE   8 //color before line deleted
#define GRAY    9 //color on game over

// Block Shape (0=0, 1=90, 2=180, 3=270, counter clock wise: left)
typedef char Shape[4][4];

Shape
shape_z[4] = {
 {{0,0,0,0},
  {0,0,0,0},
  {0,0,0,0},
  {0,0,0,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {0,0,0,0},
  {0,0,0,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {0,0,0,0},
  {0,0,0,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {0,0,0,0},
  {0,0,0,0}}};

Shape
shape_a[4] = {
 {{0,0,0,0},
  {0,0,0,0},
  {0,1,1,0},
  {0,1,1,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {0,1,1,0},
  {0,1,1,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {0,1,1,0},
  {0,1,1,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {0,1,1,0},
  {0,1,1,0}}};

Shape
shape_b[4] = {
 {{0,1,0,0},
  {0,1,0,0},
  {0,1,0,0},
  {0,1,0,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {1,1,1,1},
  {0,0,0,0}},

 {{0,1,0,0},
  {0,1,0,0},
  {0,1,0,0},
  {0,1,0,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {1,1,1,1},
  {0,0,0,0}}};

Shape
shape_c[4] = {
 {{0,0,0,0},
  {0,1,0,0},
  {1,1,1,0},
  {0,0,0,0}},

 {{0,0,0,0},
  {0,1,0,0},
  {1,1,0,0},
  {0,1,0,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {1,1,1,0},
  {0,1,0,0}},

 {{0,0,0,0},
  {0,1,0,0},
  {0,1,1,0},
  {0,1,0,0}}};

Shape
shape_d[4] = {
 {{0,0,0,0},
  {0,0,0,0},
  {1,1,0,0},
  {0,1,1,0}},

 {{0,0,0,0},
  {0,0,1,0},
  {0,1,1,0},
  {0,1,0,0}},

 {{0,0,0,0},
  {1,1,0,0},
  {0,1,1,0},
  {0,0,0,0}},

 {{0,0,0,0},
  {0,1,0,0},
  {1,1,0,0},
  {1,0,0,0}}};

Shape
shape_e[4] = {
 {{0,0,0,0},
  {0,0,0,0},
  {0,1,1,0},
  {1,1,0,0}},

 {{0,0,0,0},
  {0,1,0,0},
  {0,1,1,0},
  {0,0,1,0}},

 {{0,0,0,0},
  {0,1,1,0},
  {1,1,0,0},
  {0,0,0,0}},

 {{0,0,0,0},
  {1,0,0,0},
  {1,1,0,0},
  {0,1,0,0}}};

Shape
shape_f[4] = {
 {{0,0,0,0},
  {0,1,0,0},
  {0,1,0,0},
  {0,1,1,0}},

 {{0,0,0,0},
  {0,0,1,0},
  {1,1,1,0},
  {0,0,0,0}},

 {{0,0,0,0},
  {1,1,0,0},
  {0,1,0,0},
  {0,1,0,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {1,1,1,0},
  {1,0,0,0}}};

Shape
shape_g[4] = {
 {{0,0,0,0},
  {0,0,1,0},
  {0,0,1,0},
  {0,1,1,0}},

 {{0,0,0,0},
  {0,0,0,0},
  {0,1,1,1},
  {0,0,0,1}},

 {{0,0,0,0},
  {0,0,1,1},
  {0,0,1,0},
  {0,0,1,0}},

 {{0,0,0,0},
  {0,1,0,0},
  {0,1,1,1},
  {0,0,0,0}}};

// Block (made out of 4 squares)
typedef struct {
  int x;
  int y;
  int rot;
  int color;
  Shape s;
} block;

// Returns the numbered Color
int getColor(SDL_Surface *target, char color) {
  unsigned char r = 0, g = 0, b = 0;

  switch(color) {
    case NONE:
      r = 32; g = 32; b = 32;
      break;

    case RED:
      r = 200; g = 0; b = 0;
      break;

    case ORANGE:
      r = 255; g = 128; b = 0;
      break;

    case YELLOW:
      r = 255; g = 255; b = 0;
      break;

    case GREEN:
      r = 0; g = 255; b = 0;
      break;

    case CYAN:
      r = 0; g = 200; b = 255;
      break;

    case BLUE:
      r = 0; g = 64; b = 200;
      break;

    case PURPLE:
      r = 128; g = 0; b = 128;
      break;

    case WHITE:
      r = 230; g = 230; b = 230;
      break;

    default: // GRAY
      r = 64; g = 64; b = 64;
  }

  return SDL_MapRGB(target -> format, r, g, b);
}

// Draw a Square
void draw_square(SDL_Renderer *target, SDL_Texture *blocks, SDL_Rect *size, int color) {
    SDL_Rect a, b;
    a.x = color * SQUARE_SIZE; a.y = 0; a.w = size -> w; a.h = size -> h;
    b.x = size -> x; b.y = size -> y; b.w = a.w; b.h = a.h;

    SDL_RenderCopy(target, blocks, &a, &b);
}


// Draw a Block
void draw_block(SDL_Renderer *target, SDL_Texture *blocks, block b) {
  SDL_Rect temp;
  temp.w = SQUARE_SIZE; temp.h = SQUARE_SIZE;

  int i, j;
  for(i = 0; i < 4; i++) {
    for(j = 0; j < 4; j++) {
      if(b.s[i][j]) {
        temp.x = (b.x + i) * (SQUARE_SIZE + 1) + 1;
        temp.y = (b.y + j) * (SQUARE_SIZE + 1) + 1;
        if(temp.y >= 0)
          draw_square(target, blocks, &temp, b.color);
      }
    }
  }
}


// Copy Shape
void copy_shape(Shape src, Shape dst) {
  int i, j;
  for(i = 0; i < 4; i++) {
    for(j = 0; j < 4; j++) {
      dst[j][i] = src[i][j];
    }
  }
}


// Make a Random Block
void make_random_block(block *b) {
  b -> x = 3;
  b -> y = -4;
  b -> rot = 0;

  int tmp = (rand() % 7) + 1;
  b -> color = tmp;

  switch(tmp) {
    case 1: copy_shape(shape_a[0], b -> s); break;
    case 2: copy_shape(shape_b[0], b -> s); break;
    case 3: copy_shape(shape_c[0], b -> s); break;
    case 4: copy_shape(shape_d[0], b -> s); break;
    case 5: copy_shape(shape_e[0], b -> s); break;
    case 6: copy_shape(shape_f[0], b -> s); break;
    case 7: copy_shape(shape_g[0], b -> s); break;
    default: copy_shape(shape_z[0], b -> s); break;
  }
}


// Check for Side Collision
bool check_side_coll(block bl, char bo[20][10]) {
  int i, j, ta, tb;
  for(i = 0; i < 4; i++) {
    for(j = 0; j < 4; j++) {
      if(bl.s[i][j]) {
        ta = bl.x + i;
        tb = bl.y + j;

        // Side Wall Collision
        if(ta < 0 || ta > 9)
          return true;

        // Square Collision
        if(tb >= 0 && ta >= 0 && bo[tb][ta])
          return true;
      }
    }
  }
  return false;
}


// Check for Bottom Collision
bool check_bottom_coll(block bl, char bo[20][10]) {
  int i, j, ta, tb;
  for(i = 0; i < 4; i++) {
    for(j = 0; j < 4; j++) {
      if(bl.s[i][j] != 0) {
        ta = bl.x + i;
        tb = bl.y + j;

        // Bottom Wall Collision
        if(tb > 19)
          return true;

        // Square Collision
        if(tb >= 0 && ta >= 0 && bo[tb][ta])
          return true;
      }
    }
  }
  return false;
}


// Rotate Block
void rotate_block(block *b, bool right) {
  if(right)
    b -> rot--;
  else
    b -> rot++;

  if(b -> rot < 0)
    b -> rot = 3;

  if(b -> rot > 3)
    b -> rot = 0;

  switch(b -> color) {
    case 1: copy_shape(shape_a[b -> rot], b -> s); break;
    case 2: copy_shape(shape_b[b -> rot], b -> s); break;
    case 3: copy_shape(shape_c[b -> rot], b -> s); break;
    case 4: copy_shape(shape_d[b -> rot], b -> s); break;
    case 5: copy_shape(shape_e[b -> rot], b -> s); break;
    case 6: copy_shape(shape_f[b -> rot], b -> s); break;
    case 7: copy_shape(shape_g[b -> rot], b -> s); break;
    default: copy_shape(shape_z[b -> rot], b -> s); break;
  }
}


// Merge a Block with the Board
void merge_squares(block bl, char bo[20][10]) {
  int i, j;
  for(i = 0; i < 4; i++) {
    for(j = 0; j < 4; j++) {
      if(bl.s[i][j] && (bl.y + j >= 0) && (bl.y + j < 20) && (bl.x + i >= 0) && (bl.x + i < 10))
        bo[bl.y + j][bl.x + i] = bl.color;
    }
  }
}


// Clear the Surface
void clear_surf(SDL_Renderer *target) {
    SDL_RenderClear(target);
}

// Clear the Board
void clear_board(char b[20][10]) {
  int i, j;
  for(j = 0; j < 20; j++)
    for(i = 0; i < 10; i++)
      b[j][i] = 0;
}


// Turn all the Blocks Gray (Game Over)
void gray_board(char b[20][10]) {
  int i, j;
  for(j = 0; j < 20; j++)
    for(i = 0; i < 10; i++)
      if(b[j][i])
        b[j][i] = GRAY;
}


// Turns a Line White (marks it for elimination)
void white_line(char board[20][10], int position) {
  int i;
  for(i = 0; i < 10; i++)
    board[position][i] = WHITE;
}


// Moves all the Blocks down
void slide(char board[20][10], int position) {
  int i, j;
  for(j = position; j >= 0; j--)
    for(i = 0; i < 10; i++)
      if(j == 0)
        board[j][i] = 0;
      else
        board[j][i] = board[j-1][i];
}


// Erases all the White Lines
void sweep_board(char board[20][10]) {
  int j;
  for(j = 0; j < 20; j++)
    if(board[j][0] == WHITE)
      slide(board, j);
}


// Checks for lines and slide over them
int check4lines(char board[20][10], bool *sweep) {
  int i, j, line, changes = 0;
  for(j = 0; j < 20; j++) {
    line = 0;

    for(i = 0; i < 10; i++)
      if(board[j][i])
        line++;

    if(line == 10) {
      white_line(board, j);
      *sweep = true;
      changes++;
    }
  }
  return changes;
}


// Check if the Game is Over
bool check4lose(char board[20][10]) {
  int i;
  for(i = 0; i < 10; i++)
    if(board[0][i])
      return true;
  return false;
}


// Define a SDL_Rect
void setRect(SDL_Rect *r, int x, int y, int w, int h) {
  r -> x = x;
  r -> y = y;
  r -> w = w;
  r -> h = h;
}


// Load BMP
SDL_Texture* loadbmp(SDL_Renderer *renderer, const char *file, int *size = NULL) {
  SDL_Texture *image = NULL;
  image = IMG_LoadTexture(renderer, file);
  if (size != NULL) {
      SDL_QueryTexture(image, NULL, NULL, &size[0], &size[1]);
  }
  return image;
}

const int WIN_W = 10 * SQUARE_SIZE + 11;
const int WIN_H = 20 * SQUARE_SIZE + 21;







  SDL_Window *window = NULL;
  SDL_Renderer *screen = NULL;

  // Load Images
  SDL_Texture *img_title, *img_start, *img_paused, *img_over, *img_ver, *img_blocks;

  // Score & Board Variables
  int score = 0;
  char board[20][10];
  bool sweep = false;

  // Cycle Variables (avoids making them per frame)
  int i, j;

  // Block Rect
  SDL_Rect square;

  // Update Variables
  bool update = false;
  int interval = INTERVAL;
  Uint32 counter = 0, pause_start = 0;

  SDL_Event event;
  bool done = false, pause_active = false, freeze = false; // can't have a "pause" variable as there is one such function in emscripten

  // Menu Variables and Image Location (Centered) Init
  bool menu = true, over = false;
  SDL_Rect src_rect, dst_rect;
  
  char caption[40];

  int title_x, title_y;
  int start_x, start_y;
  int paused_x, paused_y;
  int over_x, over_y;
  int version_x, version_y;


  // Block Variables
  block b;

  // FPS Counting Variables
  Uint32 fps_start = 0, fps_count = 0;
  const int fps_interval = FPS_INTERVAL / 1000;
  bool count_fps = false;

  // Difficulty Variables (0=SLOW, 1=NORM, 2=FAST)
  char diff = 0;











void loop() {
      if(freeze) {
        while(SDL_PollEvent(&event)) {
          if(event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_FOCUS_GAINED) {
            freeze = false;
            sprintf(caption, "Zetrix");
#if __EMSCRIPTEN__
            emscripten_set_window_title(caption);
#else
            SDL_SetWindowTitle(window, caption);
#endif
          }
        }
      return;
    }

    // Handle Input
    while(SDL_PollEvent(&event)) {
      switch(event.type) {
        case SDL_QUIT:
          done = true;
          break;

        case SDL_WINDOWEVENT:
          if(event.window.event == SDL_WINDOWEVENT_FOCUS_LOST) {
              sprintf(caption, "Z (Paused)");
#if __EMSCRIPTEN__
              emscripten_set_window_title(caption);
#else
              SDL_SetWindowTitle(window, caption);
#endif
              freeze = true;
          }
          break;

        case SDL_KEYDOWN:
          switch(event.key.keysym.sym) {
            case SDLK_ESCAPE: case SDLK_x:
              if(menu) {
#if !__EMSCRIPTEN__
                done = true;
#endif
              } else {
                clear_board(board);
                clear_surf(screen);
                menu = true;
                over = false;
                pause_active = false;
                sweep = false;

                sprintf(caption, "Zetrix");
#if __EMSCRIPTEN__
                emscripten_set_window_title(caption);
#else
                SDL_SetWindowTitle(window, caption);
#endif
              }
              break;

            case SDLK_1: case SDLK_2: case SDLK_3:
              if(menu) {
                if(event.key.keysym.sym == SDLK_1) {
                  diff = 0;
                  interval = INTERVAL + SLOW_DIFF;
                } else if(event.key.keysym.sym == SDLK_2) {
                  diff = 1;
                  interval = INTERVAL;
                } else if(event.key.keysym.sym == SDLK_3) {
                  diff = 2;
                  interval = INTERVAL - FAST_DIFF;
                }

                menu = false;
                over = false;
                pause_active = false;
                sweep = false;
                score = 0;
                counter = SDL_GetTicks();

                sprintf(caption, "Z (0 pts)");
#if __EMSCRIPTEN__
                emscripten_set_window_title(caption);
#else
                SDL_SetWindowTitle(window, caption);
#endif

                make_random_block(&b);
                clear_board(board);
                clear_surf(screen);
              }
              break;

            case SDLK_p:
              if(!menu && !over) {
                if(pause_active) {
                  pause_active = false;
                  clear_surf(screen);
                  counter += (SDL_GetTicks() - pause_start);

                  sprintf(caption, "Z (%d pts)", score);
#if __EMSCRIPTEN__
                  emscripten_set_window_title(caption);
#else
                  SDL_SetWindowTitle(window, caption);
#endif
                } else {
                  pause_active = true;
                  pause_start = SDL_GetTicks();

                  sprintf(caption, "Z (Paused)");
#if __EMSCRIPTEN__
                  emscripten_set_window_title(caption);
#else
                  SDL_SetWindowTitle(window, caption);
#endif
                }
              }
              break;

            case SDLK_LEFT: case SDLK_a:
              if(!menu && !over && !pause_active) {
                b.x--;
                if(check_side_coll(b, board))
                  b.x++;
              }
              break;

            case SDLK_RIGHT: case SDLK_d:
              if(!menu && !over && !pause_active) {
                b.x++;
                if(check_side_coll(b, board))
                  b.x--;
              }
              break;

            case SDLK_UP: case SDLK_w:
              if(!menu && !over && !pause_active) {
                rotate_block(&b, true);
                if(check_side_coll(b, board))
                  rotate_block(&b, false);
              }
              break;

            case SDLK_DOWN: case SDLK_s:
              if(!menu && !over && !pause_active)
                interval = SPEED_CHANGE;
              break;

            default:
              if(over) {
                clear_board(board);
                clear_surf(screen);
                menu = true;
                over = false;
                pause_active = false;
                sweep = false;

                sprintf(caption, "Zetrix");
#if __EMSCRIPTEN__
                emscripten_set_window_title(caption);
#else
                SDL_SetWindowTitle(window, caption);
#endif
              }
              break;
          }
          break;

          case SDL_KEYUP:
            switch(event.key.keysym.sym) {
              case SDLK_DOWN: case SDLK_s:
                if(!menu && !over && !pause_active) {
                  switch(diff) {
                    case 0:
                      interval = INTERVAL + SLOW_DIFF;
                      break;

                    case 1:
                      interval = INTERVAL;
                      break;

                    case 2:
                      interval = INTERVAL - FAST_DIFF;
                      break;
                      
                    default:
                      break;
                  }
                }
              break;

              default:
                break;
            }
          break;
          
          default:
            break;
      }
    }

    clear_surf(screen);

    if(menu) {
      // Draw Menu
      setRect(&src_rect, 0, 0, IMG_TITLE_SIZE[0], IMG_TITLE_SIZE[1]);
      setRect(&dst_rect, title_x, title_y, IMG_TITLE_SIZE[0], IMG_TITLE_SIZE[1]);
      SDL_RenderCopy(screen, img_title, &src_rect, &dst_rect);

      setRect(&src_rect, 0, 0, IMG_START_SIZE[0], IMG_START_SIZE[1]);
      setRect(&dst_rect, start_x, start_y, IMG_START_SIZE[0], IMG_START_SIZE[1]);
      SDL_RenderCopy(screen, img_start, &src_rect, &dst_rect);

      setRect(&src_rect, 0, 0, IMG_VERSION_SIZE[0], IMG_VERSION_SIZE[1]);
      setRect(&dst_rect, version_x, version_y, IMG_VERSION_SIZE[0], IMG_VERSION_SIZE[1]);
      SDL_RenderCopy(screen, img_ver, &src_rect, &dst_rect);
    } else {
      // Draw Squares
      for(j = 0; j < 20; j++) {
        square.y = j * (SQUARE_SIZE+1) + 1;
        for(i = 0; i < 10; i++) {
          square.x = i * (SQUARE_SIZE+1) + 1;
          draw_square(screen, img_blocks, &square, board[j][i]);
        }
      }

      // Draw Block
      draw_block(screen, img_blocks, b);

      if(pause_active) {
        // Draw "Paused"
        setRect(&src_rect, 0, 0, IMG_PAUSED_SIZE[0], IMG_PAUSED_SIZE[1]);
        setRect(&dst_rect, paused_x, paused_y, IMG_PAUSED_SIZE[0], IMG_PAUSED_SIZE[1]);
        SDL_RenderCopy(screen, img_paused, &src_rect, &dst_rect);
      } else if(over) {
        // Draw "Game Over"
        setRect(&src_rect, 0, 0, IMG_OVER_SIZE[0], IMG_OVER_SIZE[1]);
        setRect(&dst_rect, over_x, over_y, IMG_OVER_SIZE[0], IMG_OVER_SIZE[1]);
        SDL_RenderCopy(screen, img_over, &src_rect, &dst_rect);
      } else {
        // Update
        if(SDL_GetTicks() > (counter + interval)) {
          counter = SDL_GetTicks();
          update = true;
        }

        if(update) {
          update = false;

          if(sweep) {
            sweep_board(board);
            sweep = false;
          } else {
            b.y++;
            if(check_bottom_coll(b, board)) {
              b.y--;
              merge_squares(b, board);
              make_random_block(&b);
            }

            if(check4lose(board)) {
              clear_surf(screen);
              over = true;
              gray_board(board);
            } else {
              score += SCORE_TIME + check4lines(board, &sweep) * score_line;

              sprintf(caption, "Z (%d pts)", score);
#if __EMSCRIPTEN__
              emscripten_set_window_title(caption);
#else
              SDL_SetWindowTitle(window, caption);
#endif
            }
          }
        }
      }
    }

    if(count_fps) {
        fps_count++;
        if(SDL_GetTicks() > fps_start + FPS_INTERVAL) {
          printf("%d frames in %d second(s) = %d frames per second\n", fps_count, fps_interval, fps_count / fps_interval);
          fps_start = SDL_GetTicks();
          fps_count = 0;
        }
    }

    SDL_RenderPresent(screen);
}






int main (int argc, char **argv) {
#if __EMSCRIPTEN__
  emscripten_set_main_loop(loop, 0, false); // if "simulate infinite loop" was true it would freeze
#endif

  // SDL Init
  if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) == -1) {
    fprintf(stderr, "Error: Unable to Initialize SDL, %s.\n", SDL_GetError());
    return 1;
  }
  
  window = SDL_CreateWindow(
    "Zetrix",
    SDL_WINDOWPOS_UNDEFINED,
    SDL_WINDOWPOS_UNDEFINED,
    WIN_W,
    WIN_H,
    SDL_WINDOW_INPUT_FOCUS
  );
  
  if (window == NULL) {
    fprintf(stderr, "Error: Unable to create a Window, %s.\n", SDL_GetError());
    return 2;
  }

  // Caption Init
  sprintf(caption, "Zetrix");
#if __EMSCRIPTEN__
  emscripten_set_window_title(caption);
#else
  SDL_SetWindowTitle(window, caption);
#endif

  // Icon Load and Init
  SDL_Surface *img_icon = NULL;
#if !__EMSCRIPTEN__
  img_icon = SDL_LoadBMP(IMG_ICON);
  
  if(!img_icon) {
    fprintf(stderr, "Warning: Unable to load the Icon file, %s.\n", SDL_GetError());
    //return 3;
  } else {
      SDL_SetWindowIcon(window, img_icon);
  }
#endif

  // Video Mode Init
  screen = SDL_CreateRenderer(
    window,
    -1,
    SDL_RENDERER_SOFTWARE & SDL_RENDERER_PRESENTVSYNC);
  if(screen == NULL) {
    fprintf(stderr, "Error: Unable to set the Video Mode, %s.\n", SDL_GetError());
    return 3;
  }
  
  
  img_title = loadbmp(screen, (char*) IMG_TITLE, IMG_TITLE_SIZE),
  img_start = loadbmp(screen, IMG_START, IMG_START_SIZE),
  img_paused = loadbmp(screen, IMG_PAUSED, IMG_PAUSED_SIZE),
  img_over = loadbmp(screen, IMG_OVER, IMG_OVER_SIZE),
  img_ver = loadbmp(screen, IMG_VERSION, IMG_VERSION_SIZE),
  img_blocks = loadbmp(screen, IMG_BLOCKS, IMG_BLOCKS_SIZE);


  if(!img_title || !img_start || !img_paused || !img_over || !img_ver || !img_blocks) {
    fprintf(stderr, "Error: Unable to load an Image file, %s.\n", SDL_GetError());
    return 4;
  }

  if(IMG_BLOCKS_SIZE[1] != SQUARE_SIZE) {
    fprintf(stderr, "Warning: Block Image not suited for the Block Size.\n");
    return 5;
  }

  if(IMG_TITLE_SIZE[0] > WIN_W || IMG_START_SIZE[0] > WIN_W || IMG_PAUSED_SIZE[0] > WIN_W || IMG_OVER_SIZE[0] > WIN_W)
    fprintf(stderr, "Warning: Menu image(s) is\\are too big.\n");

  square.x = 0; square.y = 0;
  square.w = SQUARE_SIZE;
  square.h = SQUARE_SIZE;

  SDL_SetRenderDrawColor(screen, back_r, back_g, back_b, 255);

  counter = SDL_GetTicks();

  title_x = (WIN_W / 2) - (IMG_TITLE_SIZE[0] / 2);
  title_y = (WIN_H / 6) - (IMG_TITLE_SIZE[1] / 2);

  start_x = (WIN_W / 2) - (IMG_START_SIZE[0] / 2);
  start_y = (int) ((WIN_H / 6) * 3.5 - (IMG_START_SIZE[1] / 2));

  paused_x = (WIN_W / 2) - (IMG_PAUSED_SIZE[0] / 2);
  paused_y = (WIN_H / 6) * 3 - (IMG_PAUSED_SIZE[1] / 2);

  over_x = (WIN_W / 2) - (IMG_OVER_SIZE[0] / 2);
  over_y = (WIN_H / 6) * 3 - (IMG_OVER_SIZE[1] / 2);

  version_x = (WIN_W / 2) - (IMG_VERSION_SIZE[0] / 2);
  version_y = (WIN_H) - (IMG_VERSION_SIZE[1] * 2);

  // Clear the Screen
  clear_surf(screen);

  // Generate Random Seed
  srand(SDL_GetTicks());

  // Main Loop
#if !__EMSCRIPTEN__
  while(!done) {
      loop();
  }
  
  SDL_DestroyTexture(img_blocks);
  SDL_DestroyTexture(img_ver);
  SDL_DestroyTexture(img_over);
  SDL_DestroyTexture(img_paused);
  SDL_DestroyTexture(img_start);
  SDL_DestroyTexture(img_title);
  if (img_icon != NULL)
    SDL_FreeSurface(img_icon);
  SDL_DestroyRenderer(screen);
  SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
#endif
}